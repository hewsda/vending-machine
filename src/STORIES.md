User Story
--

1. A machine is able to delivering a product
2. Owner's machine is able to add and remove products
3. A customer is able to purchase a product

Customer Type
--

1. As a customer, i want to be able to place a payment to purchase a product
2. As a customer, i want to be able to select an available product (knowing the inventory state)
3. As a customer, i want to be able to cancel my order
4. As a customer, i want to be able to receive my product

Retailer Stories
--

1. As retailer, i want to be able to provision or remove product from a machine
2. As retailer, i want to be able to known the machine inventory
3. As retailer, i want to be able to know the price of each product

Machine Stories
--

1. As a machine, i want to be able to accept or refuse payment
2. As a machine, i want to be able to know the inventory state
3. As a machine, i want to be able to deliver a product
4. As a machine, i want to be able to give change


Functional
--

1. Domain application act a the machine interface of the application
2. Interface can be accessed as a Customer and Maintainer