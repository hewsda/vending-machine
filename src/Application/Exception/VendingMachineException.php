<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Exception;

class VendingMachineException extends \RuntimeException
{
}