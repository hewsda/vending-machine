<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Contracts;

interface Entity
{
    public function sameIdentityAs(Entity $aEntity): bool;
}