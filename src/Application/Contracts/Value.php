<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Contracts;

interface Value
{
    public function sameValueAs(Value $aValue): bool;
}