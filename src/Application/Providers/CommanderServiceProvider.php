<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Providers;

use Hewsda\Commander\Providers\CommanderServiceProvider as BaseCommander;

class CommanderServiceProvider extends BaseCommander
{
    protected $namespace = 'vending_machine';

    public function boot()
    {
        parent::boot();
    }

    protected function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../../config/routes.php', 'commander');
    }
}