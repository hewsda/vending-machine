<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Providers;

use Hewsda\Commander\Providers\RouterServiceProvider as BaseRouter;

class RouterServiceProvider extends BaseRouter
{
    protected $namespace = 'vending_machine';
}