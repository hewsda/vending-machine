<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Providers;

use Illuminate\Support\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $services = [

    ];

    public function boot()
    {
        foreach ($this->services as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}