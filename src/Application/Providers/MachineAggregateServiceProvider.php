<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Providers;

use Hewsda\NoEventStore\Provider\SimpleEventStoreServiceProvider;
use Illuminate\Support\AggregateServiceProvider;

class MachineAggregateServiceProvider extends AggregateServiceProvider
{
    protected $providers = [

        // Bus
        CommanderServiceProvider::class,

        // Domain service
        DomainServiceProvider::class,

        // Router and rçutes
        RouterServiceProvider::class,

        // Read/Write model
        ProjectorServiceProvider::class,

        // Store Collection

        // Simple Event Store
        SimpleEventStoreServiceProvider::class,
    ];
}