<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Application\Providers;

use Illuminate\Support\ServiceProvider;

class ConfigurationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../../migrations');
    }
}