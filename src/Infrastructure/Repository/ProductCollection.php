<?php

declare(strict_types=1);

namespace Hewsda\VendingMachine\Infrastructure\Repository;

use Hewsda\VendingMachine\Domain\Product\Product;
use Hewsda\VendingMachine\Domain\Product\ProductId;
use Hewsda\VendingMachine\Domain\Product\ProductList;
use Illuminate\Support\Collection;

class ProductCollection implements ProductList
{
    /**
     * @var Collection
     */
    private $products;

    /**
     * ProductCatalog constructor.
     */
    public function __construct()
    {
        $this->products = new Collection();
    }

    public function save(Product $product): void
    {
        if (!$this->products->contains($product)) {
            $this->products->push($product);
        }
    }

    public function get(ProductId $productId): ?Product
    {
        return $this->products->first(function (Product $product) use ($productId) {
            return $product->getId()->sameValueAs($productId);
        });
    }

    public function count(): int
    {
        return $this->products->count();
    }
}