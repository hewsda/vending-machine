<?php

return [
    'accountable' => [
        'service_bus' => [

            /**
             * Command bus
             */
            'command_bus' => [
                'plugins' => [
                    \Hewsda\Commander\Plugin\ServiceLocatorPlugin::class
                ],
                'router' => [
                    'routes' => [

                    ]
                ]
            ],

            /**
             * Event bus
             */
            'event_bus' => [
                'plugins' => [
                    \Hewsda\Commander\Plugin\ServiceLocatorPlugin::class,
                    \Hewsda\Commander\Plugin\OnEventStrategy::class,
                    //\Hewsda\Commander\Plugin\OnEventNameStrategy::class
                ],
                'router' => [
                    'routes' => [

                    ]
                ]
            ],

            /**
             * Query bus
             */
            'query_bus' => [
                'plugins' => [
                    \Hewsda\Commander\Plugin\ServiceLocatorPlugin::class
                ],
                'router' => [
                    'routes' => [

                    ]
                ]
            ]

        ]
    ]
];