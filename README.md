First modeling
--

1. A machine can sell many products
2. A machine can sell only one product at a time
3. A machine has his own inventory
4. A machine can accept payment
5. A machine need to advertise and give change before selling any product or abort the operation by
give back the exact money

Advantages
--

1. No concurrency in selling meaning that each transaction must
be finished before another one occurs

Steps
--

1. An machine should be in a working state before selling anything

Projections
--
1. who own and is able to provision a machine.
2. Food products has expiration date, identify every product by an unique id


Business
--

1. we should be able to audit which product ran out first to adjust 
the inventory accordingly to sells