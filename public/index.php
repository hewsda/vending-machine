<?php

require_once __DIR__ . '/../vendor/autoload.php';

// Provision product catalogue

$products = new \Hewsda\VendingMachine\Infrastructure\Repository\ProductCollection();

$list = [
    1 => 'MARS',
    2 => 'KINDER',
    3 => 'BOUNTY',
    4 => 'WATER',
    5 => 'COKE',
    6 => 'ORANGINA',
    7 => 'TOBLERONE'
];

foreach ($list as $productId => $productName) {
    $products->save(
        new \Hewsda\VendingMachine\Domain\Product\Product(
            new \Hewsda\VendingMachine\Domain\Product\ProductId($productId),
            new \Hewsda\VendingMachine\Domain\Product\ProductInfo(
                $productName,
                new \Hewsda\VendingMachine\Domain\Product\Price($productId)
            )
        )
    );
}

echo sprintf('Product catalog has been provisioned with "%s" products.<br>', $products->count());

// Configured the machine
// Provision dispenser
// Test
// Make ti functional